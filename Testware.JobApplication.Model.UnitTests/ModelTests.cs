﻿using NUnit.Framework;
using System;
using System.Globalization;
using Testware.JobApplication.Model.UnitTests.TestCases;

namespace Testware.JobApplication.ModelUnitTests
{
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    [TestFixture]
    public class ModelTests
    {
        [Test, TestCaseSource(typeof(StrictComparisonTestCases), "TestCases")]
        public bool TestEqual_StrictComparison(ModelBase a, ModelBase b)
        {
            return a.Equals(b, ComparisonModeEnum.Strict);
        }

        [Test]
        [TestCaseSource(typeof(IgnoreCaseComparisonTestCases), "TestCases")]
        public bool TestEqual_IgnoreCaseComparison(ModelBase a, ModelBase b)
        {
            return a.Equals(b, ComparisonModeEnum.IgnoreCase);
        }

        [Test]
        [TestCaseSource(typeof(RelaxedComparisonTestCases), "TestCases")]
        public bool TestEqual_RelaxedComparison(ModelBase a, ModelBase b)
        {
            return a.Equals(b, ComparisonModeEnum.Relaxed);
        }

        // We are feeding strict test cases to this test because default comparison is strict.
        [Test]
        [TestCaseSource(typeof(StrictComparisonTestCases), "TestCases")]
        public bool TestEqual_DefaultComparisonMode(ModelBase a, ModelBase b)
        {
            return a.Equals(b);
        }

        [Test]
        [TestCaseSource(typeof(CombinedModesTestCases), "TestCases")]
        public bool TestEqual_CombinedModesComparison(ModelBase a, ModelBase b)
        {
            // If all modes are combined, this will result to a strict and case-ignored comparison
            return a.Equals(b, ComparisonModeEnum.Relaxed | ComparisonModeEnum.Strict | ComparisonModeEnum.IgnoreCase);
        }

        [Test]
        public void TestToString()
        {
            var dateTimeValue = DateTime.ParseExact("6/3/2016", "M/d/yyyy", CultureInfo.CurrentCulture);
            ModelBase model = new DomainModel()
            {
                Identifier = "123",
                AppCodeAccessNumber = 134L,
                AppCodeRTGSNumber = null,
                EnrollmentDate = dateTimeValue,
                CentralAdministrator = true
            };

            string output = model.ToString();

            Assert.That(output, Is.StringStarting("Name: DomainModel"), "Must output the type name of the model.");
            
            // Iterate through the properties and test if all of them are included in the output.0
            var props = model.GetType().GetProperties();
            foreach (var p in props)
            {
                object value = p.GetValue(model, null);
                string expectedOutput = string.Format("{0}: [{1}]", p.Name, value != null ? value : "null");
                Assert.That(output, Is.StringContaining(expectedOutput), "Must output property values correctly.");
            }
        }
    }
}
