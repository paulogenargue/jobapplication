﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace Testware.JobApplication.Model.UnitTests.TestCases
{
    [ExcludeFromCodeCoverage]
    public class StrictComparisonTestCases
    {
        /// <summary>
        /// Gets all test cases for strict comparison of models.
        /// </summary>
        public static IEnumerable TestCases
        {
            get
            {
                // Equal models
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    },
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    })
                    .Returns(true)
                    .SetName("TestStrictComparison_EqualValues")
                    .SetDescription("Compare 2 models with equal values.");

                // All nulls
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = null,
                        Description = null,
                        AppCodeAccessNumber = null,
                        AppCodeRTGSNumber = null,
                        CentralAdministrator = null,
                        EnrollmentDate = null
                    },
                    new DomainModel()
                    {
                        Identifier = null,
                        Description = null,
                        AppCodeAccessNumber = null,
                        AppCodeRTGSNumber = null,
                        CentralAdministrator = null,
                        EnrollmentDate = null
                    })
                    .Returns(true)
                    .SetName("TestStrictComparison_AllNullValues")
                    .SetDescription("2 models with all null values should be equal to each other.");

                // Completely different model
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    },
                    new DomainModel()
                    {
                        Identifier = "222",
                        Description = "ModelB",
                        AppCodeAccessNumber = 222L,
                        AppCodeRTGSNumber = 456L,
                        CentralAdministrator = false,
                        EnrollmentDate = DateTime.Parse("6/4/2016")
                    })
                    .Returns(false)
                    .SetName("TestStrictComparison_CompleteyDifferentModels")
                    .SetDescription("2 completely different models should not be equal.");

                // With character case difference
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    },
                    new DomainModel()
                    {
                        Identifier = "111",
                        // Everything's the same except the case of description
                        Description = "modela",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    })
                    .Returns(false)
                    .SetName("TestStrictComparison_DifferentCharacterCasing")
                    .SetDescription("2 models with same values but diferent character cases should not be equal.");

                // With null values
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    },
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = null,
                        AppCodeAccessNumber = null,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = null,
                        EnrollmentDate = null
                    })
                    .Returns(false)
                    .SetName("TestStrictComparison_WithNullValues")
                    .SetDescription("Null values on models should not be ignored.");
            }
        }

        
    }
}
