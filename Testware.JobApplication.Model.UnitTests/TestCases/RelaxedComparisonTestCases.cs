﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;

namespace Testware.JobApplication.Model.UnitTests.TestCases
{
    [ExcludeFromCodeCoverage]
    public class RelaxedComparisonTestCases
    {
        /// <summary>
        /// Gets all test cases for relaxed comparison of models.
        /// </summary>
        public static IEnumerable TestCases
        {
            get
            {
                // Equal models
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    },
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    })
                    .Returns(true)
                    .SetName("TestRelaxedComparison_EqualValues")
                    .SetDescription("Compare 2 models with equal values.");

                // All nulls
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = null,
                        Description = null,
                        AppCodeAccessNumber = null,
                        AppCodeRTGSNumber = null,
                        CentralAdministrator = null,
                        EnrollmentDate = null
                    },
                    new DomainModel()
                    {
                        Identifier = null,
                        Description = null,
                        AppCodeAccessNumber = null,
                        AppCodeRTGSNumber = null,
                        CentralAdministrator = null,
                        EnrollmentDate = null
                    })
                    .Returns(true)
                    .SetName("TestRelaxedComparison_AllNullValues")
                    .SetDescription("2 models with all null values should be equal to each other.");

                // Completely different models
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    },
                    new DomainModel()
                    {
                        Identifier = "222",
                        Description = "ModelB",
                        AppCodeAccessNumber = 222L,
                        AppCodeRTGSNumber = 456L,
                        CentralAdministrator = false,
                        EnrollmentDate = DateTime.Parse("6/4/2016")
                    })
                    .Returns(false)
                    .SetName("TestRelaxedComparison_CompleteyDifferentModels")
                    .SetDescription("2 completely different models should not be equal.");

                // With character case difference
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    },
                    new DomainModel()
                    {
                        Identifier = "111",
                        // Everything's the same except the case of description
                        Description = "modela",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    })
                    .Returns(false)
                    .SetName("TestRelaxedComparison_DifferentCharacterCasing")
                    .SetDescription("Relaxed comparison should not mean character casing is ignored.");

                // With null values on the first model
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = null,
                        AppCodeAccessNumber = null,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = null,
                        EnrollmentDate = null
                    },
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    })
                    .Returns(true)
                    .SetName("TestRelaxedComparison_WithNullValuesOnFirstModel")
                    .SetDescription("Null values on the first model should be ignored.");

                // With null values on the second model
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    },
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = null,
                        EnrollmentDate = null
                    })
                    .Returns(false)
                    .SetName("TestRelaxedComparison_WithNullValuesOnSecondModel")
                    .SetDescription("Null values on the second model (argument to Equal method) should NOT be ignored.");

                // With all null values on the first model
                yield return new TestCaseData(
                    new DomainModel()
                    {
                        Identifier = null,
                        Description = null,
                        AppCodeAccessNumber = null,
                        AppCodeRTGSNumber = null,
                        CentralAdministrator = null,
                        EnrollmentDate = null
                    },
                    new DomainModel()
                    {
                        Identifier = "111",
                        Description = "ModelA",
                        AppCodeAccessNumber = 111L,
                        AppCodeRTGSNumber = 123L,
                        CentralAdministrator = true,
                        EnrollmentDate = new DateTime(2016, 6, 3)
                    })
                    .Returns(true)
                    .SetName("TestRelaxedComparison_WithAllNullValuesOnFirstModel")
                    .SetDescription("Null values on the first model (owner of invoked Equal method) should all be ignored.");
            }
        }
    }
}
